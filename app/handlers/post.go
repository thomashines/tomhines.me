package handlers

import (
	"fmt"
	"net/http"
	// "path/filepath"
	// "os"
	"strings"
	
	"github.com/gorilla/mux"
	"gopkg.in/validator.v2"

	"bitbucket.org/spark4160/tomhines.me/app/util"
	"bitbucket.org/spark4160/tomhines.me/app/session"
	"bitbucket.org/spark4160/tomhines.me/app/models"
)

// New
func (handler *Handler) NewPostHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("NewPostHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Check for access permission
	user := session.GetUser(handler.DB)
	if user == nil || user.UserTypeID >= 3 {
		http.Redirect(rw, req, "/", 303)
		return
	}

	// Initialise blank input
	post := new(models.Post)

	// Check if post
	if req.Method == "POST" {
		// Get the input
		models.UpdateModel(post, req)
		// Validate
		if valid := validator.Validate(post); valid != nil {
			// Add error to flashes
			session.Error(valid)
		} else {
			// Set user
			post.Author = *session.GetUser(handler.DB)
			// Create post
			handler.DB.Create(post)
			// Go to the post
			http.Redirect(rw, req, fmt.Sprintf("/post/%d", post.ID), 303)
		}
	}

	fmt.Printf("ID: %T\n", post.ID)

	handler.WriteAmber(rw, req, session, "post/edit", post)
}

// Edit
func (handler *Handler) EditPostHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("EditPostHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Check for access permission
	user := session.GetUser(handler.DB)
	if user == nil || user.UserTypeID >= 3 {
		http.Redirect(rw, req, "/", 303)
		return
	}

	// Load post
	var post models.Post
	handler.DB.Where("id = ?", mux.Vars(req)["id"]).First(&post)

	// Check if author
	if post.AuthorID != user.ID {
		http.Redirect(rw, req, "/", 303)
		return
	}

	// Check if post
	if req.Method == "POST" {
		// Get the input
		post_edits := new(models.Post)
		models.UpdateModel(post_edits, req)
		post.Title = post_edits.Title
		post.Thumbnail = post_edits.Thumbnail
		post.URL = post_edits.URL
		post.Content = post_edits.Content
		post.AccessID = post_edits.AccessID
		// Clear tags
		handler.DB.Model(&post).Association("Tags").Clear()
		// Split the tags
		for _, tag_name := range strings.Split(post_edits.TagsString, " ") {
			if len(tag_name) > 0 {
				tag := new(models.Tag)
				handler.DB.Where(models.Tag{Name: tag_name}).FirstOrInit(tag)
				if tag.ID == 0 {
					handler.DB.Save(tag)
				}
				handler.DB.Model(&post).Association("Tags").Append(tag)
			}
		}
		// Validate
		if valid := validator.Validate(post); valid != nil {
			// Add error to flashes
			session.Error(valid)
		} else {
			// Save post
			handler.DB.Save(post)
			// Announce
			session.Success("Saved post.")
		}
	}

	post.Prepare(handler.DB)

	handler.WriteAmber(rw, req, session, "post/edit", post)
	return
}

// View
func (handler *Handler) ViewPostHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("ViewPostHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Load post
	var post models.Post
	handler.DB.Where("id = ?", mux.Vars(req)["id"]).First(&post)
	post.Prepare(handler.DB)

	handler.WriteAmber(rw, req, session, "post/view", post)
}
