$(document).ready(function() {
	// Logo
	if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

	var container = $('div#logo');
	var width = container.width();
	var height = container.height();

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(60, width/height, 0.1, 1000);
	camera.position.z = 17;

	var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
	renderer.setSize(width, height);
	container.append(renderer.domElement);

	var loader = new THREE.STLLoader();
	var logo;
	loader.load( '/static/img/logo/logo.stl', function ( geometry ) {
		var material = new THREE.MeshBasicMaterial({ color: 0x303F9F });
		logo = new THREE.Mesh( geometry, material );
		logo.rotation.set(-Math.PI/2, 0, 0);
		scene.add(logo);
		renderer.render(scene, camera);
	});

	document.onmousemove = function(e) {
		var angle = 4*Math.PI*e.pageX/$(window).width();
		logo.rotation.z = angle;
		renderer.render(scene, camera);
	}
});