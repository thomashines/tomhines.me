# $make tomhines.me && tomhines.me conf.json

clean:
	go clean bitbucket.org/spark4160/tomhines.me/app
	go clean bitbucket.org/spark4160/tomhines.me

tomhines.me: clean
	go install bitbucket.org/spark4160/tomhines.me/app
	go install bitbucket.org/spark4160/tomhines.me

