package handlers

import (
	// "fmt"
	"net/http"
	// "path/filepath"
	// "os"
	// "strings"
	
	// "github.com/gorilla/mux"
	"gopkg.in/validator.v2"

	"bitbucket.org/spark4160/tomhines.me/app/util"
	"bitbucket.org/spark4160/tomhines.me/app/session"
	"bitbucket.org/spark4160/tomhines.me/app/models"
)

// Sign up
func (handler *Handler) SignUpHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("SignUpHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Initialise blank input
	user := new(models.User)

	// Check if post
	if req.Method == "POST" {
		// Get the input
		user.Update(req)
		// Validate
		if valid := validator.Validate(user); valid != nil {
			// Add error to flashes
			session.Error(valid)
		} else {
			// Check if email in use
			var duplicates []models.User
			handler.DB.Where("email = ?", user.Email).Find(&duplicates)
			if len(duplicates) != 0 {
				session.Error("Email is already in use.")
			} else if len(user.Password) == 0 {
				// Check for password
				session.Error("A password is required.")
			} else if user.Password != user.PasswordRepeat {
				// Check for password match
				session.Error("Passwords do not match.")
			} else {
				// Set user type
				user.UserTypeID = 3
				// Set password
				user.SetPassword(user.Password)
				// Create user
				handler.DB.Create(user)
				// Log in
				session.LogIn(user)
				// Save session
				session.Save(req, rw)
				// Go to the index
				http.Redirect(rw, req, "/", 303)
			}
		}
	}

	handler.WriteAmber(rw, req, session, "user/signup", user)
}

// Log in
func (handler *Handler) LogInHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("LogInHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Initialise blank input
	user := new(models.User)

	// Check if post
	if req.Method == "POST" {
		// Get the input
		user.Update(req)
		// Look up the email
		var match models.User
		handler.DB.Where("email = ?", user.Email).First(&match)
		// Check password
		if match.TestPassword(user.Password) == nil {
			// Log in
			session.LogIn(&match)
			// Save session
			session.Save(req, rw)
			// Go to the index
			http.Redirect(rw, req, "/", 303)
		} else {
			session.Error("Password incorrect or email not registered.")
		}
	}

	handler.WriteAmber(rw, req, session, "user/login", user)
}

// Log Out
func (handler *Handler) LogOutHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("LogOutHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Log out
	session.LogOut()

	// Save session
	session.Save(req, rw)

	// Go to the index
	http.Redirect(rw, req, "/", 303)
}

// Inbox
func (handler *Handler) InboxHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("InboxHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	handler.WriteAmber(rw, req, session, "user/inbox", nil)
}
