package handlers

import (
	"fmt"
	"net/http"
	"path/filepath"
	"os"
	// "strings"
	
	"github.com/gorilla/mux"
	// "gopkg.in/validator.v2"

	"bitbucket.org/spark4160/tomhines.me/app/util"
	"bitbucket.org/spark4160/tomhines.me/app/models"
	"bitbucket.org/spark4160/tomhines.me/app/session"
)

// Static files
func (handler *Handler) StaticHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("StaticHandler", req)

	// Get filename
	filename := filepath.Join("static", mux.Vars(req)["file"])
	fmt.Println(filename)
	
	// Check if it exists
	if fileinfo, err := os.Stat(filename); os.IsNotExist(err) {
		http.NotFound(rw, req)
	} else {
		// Open the file
		file, _ := os.Open(filename)

		// Serve the file
		http.ServeContent(rw, req, filename, fileinfo.ModTime(), file)
	}
}

// Index
func (handler *Handler) IndexHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("IndexHandler", req)
	
	// Get session
	session := session.GetSession(req, handler.Store)

	// Get posts
	var posts []models.Post
	user_id := session.GetUserID()
	if user_id == 0 {
		handler.DB.Where("access_id = ?", 2).Order("id desc").Find(&posts)
	} else {
		handler.DB.Where(
			"access_id = ? or author_id = ?",
			2,
			user_id).Order(
			"id desc").Find(&posts)
	}
	for i, _ := range posts {
		posts[i].Prepare(handler.DB)
	}

	handler.WriteAmber(rw, req, session, "index", posts)
}
