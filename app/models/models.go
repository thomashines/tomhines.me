package models

import (
	// "fmt"
	"net/http"
	// "strings"
	// "time"

	// "code.google.com/p/go.crypto/bcrypt"
	"github.com/gorilla/schema"
	// "github.com/jinzhu/gorm"
)

func UpdateModel(model interface{}, req *http.Request) {
	req.ParseForm()

	// Set up schema decoder
	var decoder = schema.NewDecoder()
	decoder.Decode(model, req.PostForm)
}