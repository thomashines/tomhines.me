$(document).ready(function() {
	$('.markdown').each(function() {
		var text = $(this).html();
		var converter = new Showdown.converter();
		var html = converter.makeHtml(text);
		$(this).html(html);
	})
});