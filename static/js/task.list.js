$(document).ready(function() {
	function loadChildren(index) {
		var parentid = $(this).attr("id");
		$.get("/task/"+parentid+"/children", function(data) {
			$("#"+parentid).append(data);
			$("#"+parentid+" .task").each(loadChildren);
		});
	}
	$(".task").each(loadChildren);
});