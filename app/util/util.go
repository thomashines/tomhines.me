package util

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/spark4160/tomhines.me/app/models"
)

type Data struct {
	Page string
	User *models.User
	Flashes []interface{}

	Data interface{}
}

func LogRequest(handler string, req *http.Request) {
	t := time.Now().UTC()
	fmt.Printf("[%sUTC] +%s %s %s %s %v\n",
		t.Format("20060102150405"),
		handler,
		req.Method,
		req.URL.Path,
		req.RemoteAddr,
		req.Header)
}