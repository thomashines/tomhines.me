package handlers

import (
	"fmt"
	"net/http"
	"path/filepath"

	"github.com/eknkc/amber"
	"github.com/jinzhu/gorm"
	"github.com/gorilla/sessions"

	"bitbucket.org/spark4160/tomhines.me/app/session"
	"bitbucket.org/spark4160/tomhines.me/app/util"
)

type Handler struct {
	DB *gorm.DB
	Store *sessions.CookieStore
}

func (handler *Handler) WriteAmber(rw http.ResponseWriter, req *http.Request, session *session.BlogSession, filename string, data interface{}) error {
	// Compile template
	filename = filepath.Join("views", filename + ".amber")
	fmt.Printf("Rendering %s\n", filename)
	template, err := amber.CompileFile(filename, amber.DefaultOptions)
	if err != nil {
		fmt.Printf("Failed to render: %s\n", err)
		panic(err)
		return err
	}
	
	// Get flashes
	flashes := session.Flashes();
	
	// Save session
	session.Save(req, rw)
	
	// Execute template
	return template.Execute(rw,
		util.Data{
			Page: req.URL.Path,
			User: session.GetUser(handler.DB),
			Flashes: flashes,
			Data: data})
}