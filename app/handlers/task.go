package handlers

import (
	// "container/list"
	// "encoding/json"
	"fmt"
	"net/http"
	// "os"
	"strconv"
	"time"
	"text/template"

	"github.com/gorilla/mux"
	// "github.com/jinzhu/gorm"
	"gopkg.in/validator.v2"
	
	"bitbucket.org/spark4160/tomhines.me/app/util"
	"bitbucket.org/spark4160/tomhines.me/app/session"
	"bitbucket.org/spark4160/tomhines.me/app/models"
)

// Tasks
func (handler *Handler) TasksHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("TasksHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Get tasks
	var tasks []models.Task
	user_id := session.GetUserID()
	handler.DB.Where(
		"author_id = ?", user_id).Where(
		"parent_id = ?", 0).Order(
		"tasks.end nulls last").Find(&tasks)

	for t, _ := range tasks {
		tasks[t].Prepare(handler.DB)
	}

	view := "task/tasks"
	if style, ok := mux.Vars(req)["style"]; ok {
		if style == "keep" {
			view = "task/keep"
		}
	}

	handler.WriteAmber(rw, req, session, view, tasks)
}

// New
func (handler *Handler) NewTaskHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("NewTaskHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Initialise blank input
	task := new(models.Task)

	// Check for parent
	if id, ok := mux.Vars(req)["id"]; ok {
		parentid, _ := strconv.Atoi(id)
		task.ParentID = int64(parentid)
		// Get parent
		task.LoadParent(handler.DB)
	}

	// Check if post
	if req.Method == "POST" {
		// Get the input
		models.UpdateModel(task, req)
		// Validate
		// Dates
		var err error
		if len(task.StringEnd) > 0 {
			var endtime time.Time
			endtime, err = time.Parse("2006-01-02 15:04", task.StringEnd);
			task.End = &endtime
		} else {
			task.End = nil
		}
		if valid := validator.Validate(task); valid != nil {
			// Add error to flashes
			session.Error(valid)
		} else if err != nil {
			task.End = nil
			session.Error(err)
		} else {
			// Set user
			task.AuthorID = session.GetUserID()
			fmt.Printf("Author: %d\n", task.AuthorID)
			// Create task
			handler.DB.Create(task)
			// Go to the task list
			http.Redirect(rw, req, "/tasks", 303)
		}
	}

	handler.WriteAmber(rw, req, session, "task/edit", task)
}

// Edit
func (handler *Handler) EditTaskHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("EditTaskHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Load task
	var task models.Task
	handler.DB.Where("id = ?", mux.Vars(req)["id"]).First(&task)

	// Check if author
	user := session.GetUser(handler.DB)
	if task.AuthorID != user.ID {
		http.Redirect(rw, req, "/", 303)
		return
	}

	// Check if post
	if req.Method == "POST" {
		// Get the input
		task_edits := new(models.Task)
		models.UpdateModel(task_edits, req)
		task.Title = task_edits.Title
		task.StringEnd = task_edits.StringEnd
		task.Complete = task_edits.Complete
		task.Pinned = task_edits.Pinned
		task.Content = task_edits.Content
		// Validate
		// Dates
		var err error
		if len(task.StringEnd) > 0 {
			var endtime time.Time
			endtime, err = time.Parse("2006-01-02 15:04", task.StringEnd);
			task.End = &endtime
		} else {
			task.End = nil
		}
		if valid := validator.Validate(task); valid != nil {
			// Add error to flashes
			session.Error(valid)
		} else if err != nil {
			task.End = nil
			session.Error(err)
		} else {
			// Save task
			handler.DB.Save(task)
			// Announce
			session.Success("Saved task.")
		}
	}

	task.Prepare(handler.DB)

	handler.WriteAmber(rw, req, session, "task/edit", task)
}

// Get Calendar
func (handler *Handler) ICALHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("ICALHandler", req)

	// Look up the key
	var tasks []models.Task
	if key, ok := mux.Vars(req)["key"]; ok {
		var match models.User
		handler.DB.Where("key = ?", key).First(&match)
		handler.DB.Where("author_id = ?", match.ID).Find(&tasks)
	}

	// Load tasks
	for t, _ := range tasks {
		tasks[t].Prepare(handler.DB)
		tasks[t].LoadFullTitle(handler.DB)
	}

	temp, err := template.ParseFiles("views/task/ical.ics")
	if(err != nil) {
		panic(err)
	}

	rw.Header().Set("Content-Type", "text/calendar")
	temp.Execute(rw, tasks)
}

// Get Children
func (handler *Handler) GetChildrenHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("GetChildrenHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	// Load children
	var children []models.Task
	user_id := session.GetUserID()
	handler.DB.Where(
		"author_id = ?", user_id).Where(
		"parent_id = ?", mux.Vars(req)["id"]).Order(
		"tasks.end nulls last").Find(&children)

	for c, _ := range children {
		children[c].Prepare(handler.DB)
	}

	handler.WriteAmber(rw, req, session, "task/ajax/children", children)
}
