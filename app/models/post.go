package models

import (
	// "fmt"
	// "net/http"
	// "strings"
	"time"

	// "code.google.com/p/go.crypto/bcrypt"
	// "github.com/gorilla/schema"
	"github.com/jinzhu/gorm"
)

// Tag
type Tag struct {
	ID int64
	
	Name string `sql:"size:255"`
}

// Post
type Post struct {
	ID int64
	
	Author User `schema:"-" validate:"-"`
	AuthorID int64 `schema:"-" validate:"-"`
	Title string `sql:"size:255" schema:"title" validate:"nonzero"`
	URL string `sql:"size:255" schema:"url" gorm:"column:u_r_l"`
	Thumbnail string `sql:"size:255" schema:"thumbnail"`
	Content string `sql:"type:text" schema:"content" validate:"nonzero"`
	AccessID int64 `schema:"access" validate:"-"`
	Tags []Tag `gorm:"many2many:post_tags;" schema:"-" validate:"-"`
	TagsString string `sql:"-" schema:"tags" validate:"-"`

	CreatedAt, UpdatedAt, DeletedAt time.Time `schema:"-"`
	StringCreatedAt string `sql:"-" schema:"-" validate:"-"`
}

func (post *Post) Prepare(db *gorm.DB) {
	// Load Author
	db.Model(post).Related(&post.Author, "AuthorID")

	// Load Tags
	post.Tags = []Tag{}
	db.Model(post).Related(&post.Tags, "Tags")

	// Get string
	tag_count := len(post.Tags)
	for i := 0; i < tag_count; i++ {
		post.TagsString += post.Tags[i].Name
		if i + 1 < tag_count {
			post.TagsString += " "
		}
	}
	
	// Format CreatedAt
	post.StringCreatedAt = post.CreatedAt.Format("2006/01/02 15:04 MST")
}

// Attachment
type Attachment struct {
	ID int64
	
	Author User `schema:"-"`
	AuthorID int64 `schema:"-"`
	Post Post `schema:"-"`
	PostID int64 `schema:"-"`
	File string `sql:"size:255" schema:"file" validate:"nonzero"`
	MD5 string `sql:"size:255" schema:"md5" validate:"nonzero"`

	CreatedAt, UpdatedAt, DeletedAt time.Time `schema:"-"`
	StringCreatedAt string `sql:"-" schema:"-" validate:"-"`
}

// Comment
type Comment struct {
	ID int64
	
	Author User `schema:"-"`
	AuthorID int64 `schema:"-"`
	Post Post `schema:"-"`
	PostID int64 `schema:"-"`
	ReplyToID int64 `schema:"-"`
	Content string `sql:"type:text" schema:"url"`

	CreatedAt, UpdatedAt, DeletedAt time.Time `schema:"-"`
	StringCreatedAt string `sql:"-" schema:"-" validate:"-"`
}
