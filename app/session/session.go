package session

import (
	// "fmt"
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"

	"bitbucket.org/spark4160/tomhines.me/app/models"
)

type BlogSession struct {
	*sessions.Session
}

func GetSession(req *http.Request, store *sessions.CookieStore) *BlogSession {
	session, _ := store.Get(req, "tomhines.me")
	return &BlogSession{session}
}

func (session *BlogSession) Error(message interface{}) {
	session.AddFlash(message)
}

func (session *BlogSession) Success(message interface{}) {
	session.AddFlash(message)
}

func (session *BlogSession) LogIn(user *models.User) {
	session.Values["user_id"] = user.ID
}

func (session *BlogSession) LogOut() {
	delete(session.Values, "user_id")
}

func (session *BlogSession) GetUser(db *gorm.DB) *models.User {
	user_id := session.Values["user_id"]
	if user_id == nil {
		return nil
	}
	user := new(models.User)
	db.First(user, user_id)
	return user
}

func (session *BlogSession) GetUserID() int64 {
	user_id := session.Values["user_id"]
	if user_id == nil {
		return 0
	}
	return user_id.(int64)
}

// Direct mappings
// func (session *BlogSession) Flashes() []interface{} {
// 	return session.Flashes()
// }

// func (session *BlogSession) Save(rw http.ResponseWriter, req *http.Request) error {
// 	return session.Save(rw, req)
// }