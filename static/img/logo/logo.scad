translate([ -3,  -0.5,  -5.5])
color("Red")
union() {
	translate([  0,  0,  0]) cube([ 1,  1, 12]);
	translate([ -3,  0,  0]) cube([ 4,  1,  1]);
	translate([ -3,  0,  0]) cube([ 1,  1,  6]);
	translate([ -3,  0,  5]) cube([12,  1,  1]);
	translate([  4,  0,  0]) cube([ 1,  1, 12]);
	translate([  7,  0,  0]) cube([ 1,  1, 12]);
}