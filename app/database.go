package app

import (
	"fmt"
	// "net/http"
	// "strings"
	// "time"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	// "code.google.com/p/go.crypto/bcrypt"
	// "github.com/gorilla/schema"

	"bitbucket.org/spark4160/tomhines.me/app/models"
)

func InitDB(config *Configuration) (*gorm.DB, error) {
	// Make config string
	db_string := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s sslmode=%s",
		config.DBHost,
		config.DBUser,
		config.DBPass,
		config.DBName,
		config.DBSSL)
	if config.DBPass == "" {
		db_string = fmt.Sprintf(
			"host=%s user=%s dbname=%s sslmode=%s",
			config.DBHost,
			config.DBUser,
			config.DBName,
			config.DBSSL)
	}

	// Connect
	db, err := gorm.Open(config.DBType, db_string)
	if err != nil {
		return nil, err
	}

	// Set logger
	db.LogMode(config.Verbose)

	// Return
	return db, nil
}

func Migrate(db *gorm.DB) {
	// Migrate Auth
	db.AutoMigrate(
		&models.User{},
		&models.UserType{})
	// Add user types
	db.Create(&models.UserType{1, "Admin"})
	db.Create(&models.UserType{2, "Approved"})
	db.Create(&models.UserType{3, "New"})

	// Migrate Blog
	db.AutoMigrate(
		&models.Post{},
		&models.Attachment{},
		&models.Comment{},
		&models.Tag{})
	
	// Migrate Tasks
	db.AutoMigrate(
		&models.TaskType{},
		&models.Priority{},
		&models.Project{},
		&models.Task{},
		&models.TaskAssignment{})
	// Add global task types
	db.Create(&models.TaskType{
		ID: 1,
		Name: "Collection",
		Global: true,
	})
	db.Create(&models.TaskType{
		ID: 2,
		Name: "Task",
		Global: true,
	})
	db.Create(&models.TaskType{
		ID: 3,
		Name: "Note",
		Global: true,
	})
	// Add global priorities
	db.Create(&models.Priority{
		ID: 1,
		Name: "Now",
		Rank: 1,
		Global: true,
	})
	db.Create(&models.Priority{
		ID: 2,
		Name: "Later",
		Rank: 2,
		Global: true,
	})
}
