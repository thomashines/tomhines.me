package app

import (
	"encoding/json"
	"fmt"
	// "net"
	"net/http"
	// "path/filepath"
	"os"
	// "time"

	// "github.com/eknkc/amber"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	// "github.com/gorilla/schema"
	"github.com/gorilla/sessions"

	// "bitbucket.org/spark4160/tomhines.me/app/util"
	// "bitbucket.org/spark4160/tomhines.me/app/models"
	"bitbucket.org/spark4160/tomhines.me/app/handlers"
)

type Configuration struct {
	BindTo string
	DBType string
	DBHost string
	DBUser string
	DBPass string
	DBName string
	DBSSL string
	Migrate bool
	Verbose bool
}

type App struct {
	Config *Configuration
	DB *gorm.DB
	Store *sessions.CookieStore
}

func NewApp(config_filename string, migrate bool, verbose bool) (*App, error) {
	// Make new app object
	app := new(App)

	// Load configuration
	config_file, _ := os.Open(config_filename)
	config_decoder := json.NewDecoder(config_file)
	app.Config = new(Configuration)
	err := config_decoder.Decode(app.Config)
	if err != nil {
		fmt.Println("Configuration error:", err)
		return nil, err
	}

	app.Config.Migrate = migrate
	app.Config.Verbose = verbose

	return app, nil
}

func (app *App) Serve() {
	// Log start
	cwd, _ := os.Getwd()
	fmt.Printf("Serving %s on http://%s/\n", cwd, app.Config.BindTo)
	
	// Connect to DB
	var err error
	if app.DB, err = InitDB(app.Config); err != nil {
		panic(err)
	}
	// Migrate DB
	if app.Config.Migrate {
		Migrate(app.DB)
	}

	// Init cookie store
	app.Store = sessions.NewCookieStore([]byte("tomhines.me"))

	// Set up handler
	handler := new(handlers.Handler)
	handler.DB = app.DB
	handler.Store = app.Store

	// Set up routing
	router := mux.NewRouter()

	// Routes
	
	// Static
	router.HandleFunc("/static/{file:[A-z./]+}", handler.StaticHandler)

	// Auth
	// Sign up
	router.HandleFunc("/user/signup", handler.SignUpHandler)
	// Log in
	router.HandleFunc("/user/login", handler.LogInHandler)
	// Log out
	router.HandleFunc("/user/logout", handler.LogOutHandler)
	// Inbox
	router.HandleFunc("/user/inbox", handler.InboxHandler)

	// App
	// Index
	router.HandleFunc("/", handler.IndexHandler)
	// New Post
	router.HandleFunc("/post/new", handler.NewPostHandler)
	// Edit Post
	router.HandleFunc("/post/{id}/edit", handler.EditPostHandler)
	// View Post
	router.HandleFunc("/post/{id}", handler.ViewPostHandler)

	// Task
	// Tasks
	router.HandleFunc("/tasks", handler.TasksHandler)
	router.HandleFunc("/tasks/{style}", handler.TasksHandler)
	// New Task
	router.HandleFunc("/task/new", handler.NewTaskHandler)
	router.HandleFunc("/task/{id}/new", handler.NewTaskHandler)
	// Edit Task
	router.HandleFunc("/task/{id}/edit", handler.EditTaskHandler)
	// ICAL: Calendar
	router.HandleFunc("/tasks/{key}/ical.ics", handler.ICALHandler)
	// AJAX: Get children
	router.HandleFunc("/task/{id}/children", handler.GetChildrenHandler)

	// Misc
	// Hello Internet!
	router.HandleFunc("/hi", handler.HIHandler)

	// Set up server
	// http.ServeType = "tcp4"
	fmt.Println("Serving")
	http.ListenAndServe(app.Config.BindTo, context.ClearHandler(router))
	// listener, _ := net.Listen("tcp4", host)
	// http.Serve(listener, context.ClearHandler(router))
}
