$(document).ready(function() {
	jQuery('.datetimepicker').datetimepicker({
		format:'Y-m-d H:i',
		step: 5,
		startDate: new Date()
	});
});