package main

import (
	"fmt"
	"os"

	"bitbucket.org/spark4160/tomhines.me/app"
)

func main() {
	if len(os.Args) >= 2 {
		migrate := false
		verbose := false
		config_filename := os.Args[len(os.Args) - 1]
		for _, setting := range os.Args {
			if setting == "-m" {
				migrate = true
			} else if setting == "-v" {
				verbose = true
			}
		}
		tomhines, err := app.NewApp(config_filename, migrate, verbose)
		if err != nil {
			fmt.Println("Unable to start server.")
		} else {
			tomhines.Serve()
		}
	} else {
		fmt.Println("Usage: tomhines.me <config_filename>")
	}
}
