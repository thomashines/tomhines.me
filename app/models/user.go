package models

import (
	// "fmt"
	"net/http"
	// "strings"
	"time"

	"golang.org/x/crypto/bcrypt" 
	// "github.com/gorilla/schema"
	// "github.com/jinzhu/gorm"
)

type User struct {
	ID int64
	
	Email string `sql:"size:255" schema:"email" validate:"nonzero"`
	Password string `sql:"size:255" schema:"password"`
	PasswordRepeat string `sql:"-" schema:"passwordrepeat"`
	Tag string `sql:"size:255" schema:"username" validate:"nonzero"`
	UserType UserType `schema:"-"`
	UserTypeID int64 `schema:"-"`
	Key string `sql:"size:255" schema:"-"`

	CreatedAt, UpdatedAt, DeletedAt time.Time `schema:"-"`
	StringCreatedAt string `sql:"-" schema:"-" validate:"-"`
}

func (user *User) Update(req *http.Request) {
	UpdateModel(user, req)
}

func (user *User) SetPassword(password string) {
	// Hash password
	password_bytes, _ := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)

	// Set password
	user.Password = string(password_bytes)
}

func (user *User) TestPassword(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
}

func (user *User) IsUserType(usertype_ids ...int64) bool {
	for _, usertype_id := range usertype_ids {
		if user.UserType.ID == usertype_id {
			return true
		}
	}
	return false
}

func (user *User) Prepare() {
}

func MakeUser(req *http.Request) *User {
	user := new(User)
	user.Update(req)
	return user
}

// Usertype
type UserType struct {
	ID int64
	
	Name string `sql:"size:255"`
}
