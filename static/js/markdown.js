$(document).ready(function() {
	// Markdown
	$('.markdown').each(function() {
		var text = $(this).html();
		text = text.replace(/&amp;/g, "&");
		var converter = new Showdown.converter();
		var html = converter.makeHtml(text);
		$(this).html(html);
	})
});