package handlers

import (
	// "container/list"
	// "encoding/json"
	// "fmt"
	"net/http"
	// "os"
	// "strconv"
	// "time"
	// "text/template"

	// "github.com/gorilla/mux"
	// "github.com/jinzhu/gorm"
	// "gopkg.in/validator.v2"
	
	"bitbucket.org/spark4160/tomhines.me/app/util"
	"bitbucket.org/spark4160/tomhines.me/app/session"
	// "bitbucket.org/spark4160/tomhines.me/app/models"
)

// Tasks
func (handler *Handler) HIHandler(rw http.ResponseWriter, req *http.Request) {
	util.LogRequest("HIHandler", req)

	// Get session
	session := session.GetSession(req, handler.Store)

	view := "misc/hi"
	handler.WriteAmber(rw, req, session, view, nil)
}
