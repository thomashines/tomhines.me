package models

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

// Task Type
type TaskType struct {
	ID int64

	Name string `sql:"size:255"`

	Author User
	AuthorID int64

	Global bool
}

// Priority
type Priority struct {
	ID int64

	Name string `sql:"size:255"`
	Rank int32

	Author User
	AuthorID int64

	Global bool
}

// Project
type Project struct {
	ID int64
	
	Name string `sql:"size:255"`

	Members []User `gorm:"many2many:project_members;"`
}

type Task struct {
	ID int64
	CreatedAt time.Time `schema:"-" validate:"-"`
	UpdatedAt time.Time `schema:"-" validate:"-"`
	DeletedAt *time.Time `schema:"-" validate:"-"`

	TaskType TaskType `schema:"-" validate:"-"`
	TaskTypeID int64 `schema:"-" validate:"-"`

	Parent *Task `schema:"-" validate:"-"`
	ParentID int64 `schema:"-" validate:"-"`

	Children []*Task `schema:"-" validate:"-"`

	Title string `sql:"size:255" schema:"title" validate:"nonzero"`
	FullTitle string `sql:"-" schema:"-" validate:"-"`
	Content string `sql:"type:text" schema:"content"`
	URL string `sql:"size:255" schema:"url" gorm:"column:u_r_l"`

	Author User `sql:"-" schema:"-" validate:"-"`
	AuthorID int64 `schema:"-" validate:"-"`

	Project Project `schema:"-" validate:"-"`
	ProjectID int64 `schema:"-" validate:"-"`
	
	Start time.Time `schema:"start" validate:"-"`
	StringStart string `sql:"-" schema:"-"`
	End *time.Time `schema:"-" validate:"-"`
	StringEnd string `sql:"-" schema:"end"`
	Complete bool `schema:"complete"`

	Pinned bool `schema:"pinned"`

	Priority Priority `schema:"-" validate:"-"`
	PriorityID int64 `schema:"-" validate:"-"`
	
	Hours float32 `schema:"-" validate:"-"`
	
	Level int `sql:"-" schema:"-" validate:"-"`
	Style string `sql:"-" schema:"-" validate:"-"`
}

func (task *Task) EndEnd() time.Time {
	return task.End.Add(time.Minute)
}

func (task *Task) LoadFullTitle(db *gorm.DB) {
	if len(task.FullTitle) == 0 {
		if task.ParentID == 0 {
			task.FullTitle = task.Title
		} else {
			task.LoadParent(db)
			task.Parent.LoadFullTitle(db)
			task.FullTitle = fmt.Sprintf("%s: %s", task.Parent.FullTitle, task.Title)
		}
	}
}

func (task *Task) LoadParent(db *gorm.DB) {
	parent := new(Task)
	db.First(parent, task.ParentID)
	task.Parent = parent
}

func (task *Task) LoadChildren(db *gorm.DB) {
	// Load Children
	db.Where("parent_id = ?", task.ID).Find(&task.Children)
	// Prepare Children
	fmt.Printf("Parent: %d\n", task.ID)
	for c, _ := range task.Children {
		fmt.Printf("Child: %d\n", task.Children[c].ID)
		task.Children[c].Prepare(db)
	}
}

func (task *Task) Prepare(db *gorm.DB) {
	// Load Task Type
	db.Model(task).Related(&task.TaskType, "TaskTypeID")
	// Load Parent
	task.LoadParent(db)
	// Load Author
	db.Model(task).Related(&task.Author, "AuthorID")
	// Load Project
	db.Model(task).Related(&task.Project, "ProjectID")
	// Load Priority
	db.Model(task).Related(&task.Priority, "PriorityID")

	// Format Dates
	if task.End != nil {
		task.StringEnd = task.End.Format("2006-01-02 15:04")
	}
}

// Task Assignment
type TaskAssignment struct {
	ID int64

	Task Task
	TaskID int64

	Assignee User
	AssigneeID int64

	Hours float32
}
